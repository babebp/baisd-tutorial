import timeit

def fibo_recur(n):
    if n < 2:
        return n
    else:
        return fibo_recur(n - 1) + fibo_recur(n - 2)


def fibo_bottom_up(n):
    f0 = 0
    f1 = 1
    f2 = 1
    for i in range(2, n+1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    print(
        "recursive fibonacci: ",
        fibo_recur(10),
        timeit.timeit("fibo_recur(10)", globals=globals()),
    )
    print(
        "b2u fibonacci: ",
        fibo_bottom_up(10),
        timeit.timeit("fibo_bottom_up(10)", globals=globals()),
    )