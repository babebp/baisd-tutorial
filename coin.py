def coin(amount):
    one_baht = 0
    two_baht = 0
    four_baht = 0
    five_baht = 0
    ten_baht = 0

    while amount != 0:
        if amount >= 10:
            amount -= 10
            ten_baht += 1
        elif amount >= 5:
            amount -= 5
            five_baht += 1
        elif amount >= 4:
            amount -= 4
            four_baht += 1
        elif amount >= 2:
            amount -= 2
            two_baht += 1
        elif amount >= 1:
            amount -= 1
            one_baht += 1

    return (f"10 Baht = {ten_baht} coin\n5 Baht = {five_baht} coin\n4 Baht = {four_baht} coin\n2 Baht = {two_baht} coin\n1 Baht = {one_baht} coin")
    #return ten_baht, five_baht, four_baht, two_baht, one_baht

if __name__ == "__main__":
    amount = int(input("Amount = "))
    print(coin(amount))