def selection_sort(data):
    sorted_data = data.copy()
    for i in range(len(data) -1, 0, -1):
        max_num = 0

        for j in range(1, i + 1):
            if sorted_data[j] > sorted_data[max_num]:
                max_num = j

            sorted_data[i], sorted_data[max_num] = sorted_data[max_num], sorted_data[i]

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter 10 number: ").split()))

    sorted_data = selection_sort(data)

    print(sorted_data)
